package ir.bontech.bontechapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BontechApplication {

    public static void main(String[] args) {
        SpringApplication.run(BontechApplication.class, args);
    }

}
