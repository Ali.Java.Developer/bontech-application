package ir.bontech.bontechapplication.service;

import ir.bontech.bontechapplication.data.Constraint;
import ir.bontech.bontechapplication.entity.ServicePack;
import ir.bontech.bontechapplication.repository.ServicePackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;

@Service
public class ServicePackService {

    @Autowired
    private ServicePackRepository servicePackRepository;

    @Autowired
    private UserService userService;

    public ServicePack saveNewServicePack(ServicePack servicePack, String token) {
        ServicePack savedServicePack = null;
        if (userService.isAdmin(token)) {
            if (servicePack != null) {
                servicePack.setActive(false);//in create service part you cant active service pack
                savedServicePack = servicePackRepository.save(servicePack);
            }
        }
        return savedServicePack;
    }

    public ServicePack saveServicePack(Long id, ServicePack servicePack) {
        ServicePack savedServicePack;
        try {
            servicePack.setId(findServicePackById(id).getId());
            savedServicePack = servicePackRepository.save(servicePack);
        } catch (NullPointerException e) {
            savedServicePack = null;
            e.printStackTrace();
        }
        return savedServicePack;
    }

    public ServicePack editServicePack(Long id, ServicePack servicePack, String token) {
        ServicePack editedServicePack = null;
        if (servicePack != null && findServicePackById(id) != null && userService.isAdmin(token)) {
            servicePack.setActive(false);//in edit part you cant active service pack
            servicePack.setAllowed(false);//in edit part you cant set allowed true service pack
            editedServicePack = saveServicePack(id, servicePack);
        }
        return editedServicePack;
    }

    public ServicePack findServicePackById(Long id) {
        ServicePack foundServicePack = null;
        Optional<ServicePack> optionalServicePack = servicePackRepository.findById(id);
        if(optionalServicePack.isPresent()){
            foundServicePack = optionalServicePack.get();
        }
        return foundServicePack;
    }

    public ServicePack deleteServicePack(Long id, String token) {
        ServicePack deletedServicePack = null;
        ServicePack foundServicePack = findServicePackById(id);
        if (foundServicePack != null && userService.isAdmin(token)) {
            servicePackRepository.delete(foundServicePack);
            deletedServicePack = foundServicePack;
        }
        return deletedServicePack;
    }

    public List<ServicePack> findAll() {
        return servicePackRepository.findAll();
    }

    public ServicePack setExpiredTime(Long id, String activeAt, int hour, String token) {
        ServicePack changedExpiredServicePack = null;
        try {
            if (userService.isAdmin(token)) {
                if (isAfterCurrentTime(activeAt)) {
                    ServicePack foundServicePack = findServicePackById(id);
                    if (foundServicePack.getExpiredAt() == null) {
                        foundServicePack.setActiveAt(activeAt);
                        foundServicePack.setActive(true);
                        foundServicePack.setExpiredAt(calculateExpiredAt(activeAt, hour));
                        changedExpiredServicePack = saveServicePack(id, foundServicePack);
                    } else {
                        LocalDateTime ServicePackExpiredAt = stringToLocalDateTime(foundServicePack.getExpiredAt());
                        LocalDateTime servicePackWillBeActiveAt = stringToLocalDateTime(activeAt);
                        Long timestampServicePackExpiredAt = localDateTimeToLongTimeStamp(ServicePackExpiredAt);
                        Long timestampServicePackWillBeActiveAt = localDateTimeToLongTimeStamp(servicePackWillBeActiveAt);
                        if (isSuitableTimeToSetActive(timestampServicePackWillBeActiveAt, timestampServicePackExpiredAt)) {
                            foundServicePack.setActiveAt(activeAt);
                            foundServicePack.setActive(true);
                            foundServicePack.setExpiredAt(calculateExpiredAt(activeAt, hour));
                            changedExpiredServicePack = saveServicePack(id, foundServicePack);
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            changedExpiredServicePack = null;
            e.printStackTrace();
        }
        return changedExpiredServicePack;
    }

    public String calculateExpiredAt(String activeAt, int hour) {
        Long longActiveAt = localDateTimeToLongTimeStamp(stringToLocalDateTime(activeAt));
        Long expiredAt = longActiveAt + ((hour * 60 * 59) * 1000);
        return longTimeToString(expiredAt);
    }

    public String longTimeToString(Long time) {
        String timeString = null;
        if (time != null) {
            Timestamp timestamp = new Timestamp(time);
            LocalDateTime localDateTime = timestamp.toLocalDateTime();
            timeString = localeDateTimeToString(localDateTime);
        }
        return timeString;
    }

    public String localeDateTimeToString(LocalDateTime localDateTime) {
        String time = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (localDateTime != null) {
            time = localDateTime.format(formatter);
        }
        return time;
    }

    public LocalDateTime stringToLocalDateTime(String time) {
        LocalDateTime localDateTime;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            localDateTime = LocalDateTime.parse(time, formatter);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            localDateTime = null;
        } catch (NullPointerException e) {
            e.printStackTrace();
            localDateTime = null;
        }
        return localDateTime;
    }

    public Long localDateTimeToLongTimeStamp(LocalDateTime localDateTime) {
        Long timeLong = null;
        if (localDateTime != null) {
            timeLong = Timestamp.valueOf(localDateTime).getTime();
        }
        return timeLong;
    }

    public boolean isSuitableTimeToSetActive(Long activeAt, Long PreviousExpiredOfServicePack) {
        boolean isSuitableTimeToSetActiveTime = false;
        try {
            if (activeAt - PreviousExpiredOfServicePack > Constraint.limitTimeToActiveServicePack) {
                isSuitableTimeToSetActiveTime = true;
            }
        } catch (NullPointerException e) {
            isSuitableTimeToSetActiveTime = false;
            e.printStackTrace();
        }
        return isSuitableTimeToSetActiveTime;
    }

    public boolean isAfterCurrentTime(String activeAt){
        boolean isPassedTheTime = false;
        LocalDateTime activeAtLocalDateTime = stringToLocalDateTime(activeAt);
        LocalDateTime current = getCurrentTime();
        try{
            if(current.isBefore(activeAtLocalDateTime)){
                isPassedTheTime = true;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return isPassedTheTime;
    }


    public ServicePack disableServicePack(Long id, String token) {
        ServicePack disabledService = null;
        try {
            if (userService.isAdmin(token)) {
                ServicePack foundServicePack = findServicePackById(id);
                foundServicePack.setActive(false);
                disabledService = foundServicePack;
            }
        } catch (NullPointerException e) {
            disabledService = null;
            e.printStackTrace();
        }
        return disabledService;
    }

    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    public List<ServicePack> findServicePackByActive(boolean isActive) {
        List<ServicePack> servicePackList = null;
        try {
            servicePackList = servicePackRepository.findByActive(isActive);
        } catch (NullPointerException e) {
            servicePackList = null;
            e.printStackTrace();
        }
        return servicePackList;
    }

    public List<ServicePack> findServicePackByAllowed(boolean isAllowed){
        List<ServicePack> servicePackList = null;
        try{
            servicePackList = servicePackRepository.findByAllowed(isAllowed);
        } catch (NullPointerException e) {
            servicePackList = null;
            e.printStackTrace();
        }
        return servicePackList;
    }

    @Scheduled(fixedRate = 1 * 59 * 59 * 1000)
    public void activeService() {
        try {
            List<ServicePack> activeServicePacks = servicePackRepository.findByActive(true);
            for(ServicePack item : activeServicePacks){
                LocalDateTime activeTime = stringToLocalDateTime(item.getActiveAt());
                LocalDateTime current = getCurrentTime();
                if (activeTime.isAfter(current) || activeTime.equals(current)){
                    item.setAllowed(true);
                    saveServicePack(item.getId() , item);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(fixedRate = 1 * 59 * 59 * 1000)
    public void disableService() {
        try {
            List<ServicePack> activeServicePacks = servicePackRepository.findByActive(true);
            for(ServicePack item : activeServicePacks){
                LocalDateTime expiredTime = stringToLocalDateTime(item.getExpiredAt());
                LocalDateTime current = getCurrentTime();
                if (current.isAfter(expiredTime)){
                    item.setActive(false);
                    item.setAllowed(false);
                    saveServicePack(item.getId() , item);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


}
