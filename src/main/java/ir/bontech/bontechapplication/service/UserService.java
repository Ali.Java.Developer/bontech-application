package ir.bontech.bontechapplication.service;

import com.google.gson.Gson;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import ir.bontech.bontechapplication.data.PayLoad;
import ir.bontech.bontechapplication.data.Secret;
import ir.bontech.bontechapplication.entity.User;
import ir.bontech.bontechapplication.enums.Role;
import ir.bontech.bontechapplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final Key key = Secret.key;

    @Autowired
    private UserRepository userRepository;

    public User saveUser(Long id, User user, String token) {
        User savedUser = null;
        if (isAdmin(token)) {
            if (user != null) {
                User foundUser = findUserById(id);
                if (foundUser != null) {
                    user.setId(foundUser.getId());
                    List<String> roles = getRolesOfUserByUserObject(user);
                    if(roles.size() < 1){
                        user.setRoles(addUserRoleTouserBydefault(user));
                    }
                    savedUser = userRepository.save(user);
                }
            }
        }
        return savedUser;
    }

    public User findUserByNameAndPassword(String name, String password) {
        User foundUser = null;
        Optional<User> optionalUser = userRepository.findUserByNameAndPassword(name, password);
        if (optionalUser.isPresent()) {
            foundUser = optionalUser.get();
        }
        return foundUser;
    }

    public User findUserById(Long id) {
        User foundUser = null;
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            foundUser = optionalUser.get();
        }
        return foundUser;
    }

    public String payloadToGson(User user) {
        Long id = user.getId();
        String name = user.getName();
        PayLoad payLoad = new PayLoad(id, name);
        return new Gson().toJson(payLoad);
    }

    public String generateToken(User user) {
        String name = user.getName();
        String password = user.getPassword();
        User foundUser = findUserByNameAndPassword(name, password);
        String token = null;
        if (foundUser != null) {
            String payload = payloadToGson(foundUser);
            token = Jwts.builder().setPayload(payload).signWith(key).compact();
        }
        return token;
    }

    public User findByToken(String token) {
        User foundUser = null;
        try {
            Jws<Claims> claimsJws = Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token);
            Integer integerId = (Integer) claimsJws.getBody().get("id");
            Long id = Long.valueOf(integerId);
            foundUser = findUserById(id);
        } catch (ExpiredJwtException e) {
            e.printStackTrace();
        } catch (UnsupportedJwtException e) {
            e.printStackTrace();
        } catch (MalformedJwtException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return foundUser;
    }

    public List<String> getRolesOfUserByUserObject(User user) {
        Optional<User> optionalUser = userRepository.findUserByName(user.getName());
        List<String> nameOfFoundRoles = new ArrayList<>();
        if (optionalUser.isPresent()) {
            User foundUser = optionalUser.get();
            for (Role item : foundUser.getRoles()) {
                nameOfFoundRoles.add(item.name());
            }
        }
        return nameOfFoundRoles;
    }

    public List<String> getRolesOfUserByToken(String token) {
        User foundUser = findByToken(token);
        List<String> nameOfFoundRoles = new ArrayList<>();
        if (foundUser != null) {
            for (Role item : foundUser.getRoles()) {
                nameOfFoundRoles.add(item.name());
            }
        }
        return nameOfFoundRoles;
    }

    public boolean isAdmin(String token) {
        boolean isAdmin = false;
        try {
            List<String> roles = new ArrayList<>(getRolesOfUserByToken(token));
            isAdmin = false;
            for (String item : roles) {
                if (item.equalsIgnoreCase("admin")) {
                    isAdmin = true;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return isAdmin;
    }

    public User saveNewUser(User user, String token) {
        User savedUser = null;
        if (isAdmin(token)) {
            if (user != null) {
                List<Role> roles = new ArrayList<>();
                roles.add(Role.USER);
                user.setRoles(roles);
                savedUser = userRepository.save(user);
            }
        }
        return savedUser;
    }
    public List<Role> addUserRoleTouserBydefault(User user){
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        user.setRoles(roles);
        return roles;
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public User deleteUser(Long id , String token){
        User founUser = null;
        if(isAdmin(token)){
            userRepository.deleteById(id);
            founUser = findUserById(id);
        }
        return founUser;
    }
}
