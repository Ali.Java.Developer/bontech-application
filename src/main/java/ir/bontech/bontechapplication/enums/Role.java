package ir.bontech.bontechapplication.enums;

import java.io.Serializable;

public enum Role implements Serializable {
    ADMIN,
    USER
}
