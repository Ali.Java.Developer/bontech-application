package ir.bontech.bontechapplication.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jdk.nashorn.internal.objects.annotations.Property;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "service_pack")
@Getter
@Setter
public class ServicePack {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false)
    @NotBlank(message = "name of service pack must not be blank!")
    private String name;

    @Column(nullable = false)
    @NotNull(message = "cost of service pack must not be blank!")
    private int cost;

    @Column(name = "is_active")
    private boolean isActive = false;

    @Column(name = "is_allowed")
    private boolean isAllowed = false;

    @Column(name = "hour_active")
    private int hourActive;

    @Column(name = "active_at")
    private String activeAt;

    @Column(name = "expired_at")
    private String expiredAt;



    @Column(nullable = false)
    @Property(name = "max")
    @NotNull(message = "maximum number of time to use of service pack must not be blank!")
    private int maximumNumberOfTimesToUse;

    @OneToOne(mappedBy = "servicePack" , cascade = CascadeType.ALL)
    private UserServicePackage userServicePackage;

}
