package ir.bontech.bontechapplication.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.bontech.bontechapplication.enums.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false , unique = true)
    @NotBlank(message = "username must not be blank")
    private String name;

    @Column(nullable = false)
    @NotBlank(message = "password must not be blank")
    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role" , joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role")
    @NotNull(message = "Role of user must not be null!")
    @Enumerated(EnumType.STRING)
    private List<Role> roles;

    @OneToMany(mappedBy = "user" , cascade = CascadeType.ALL)
    private List<UserServicePackage> packages;

}
