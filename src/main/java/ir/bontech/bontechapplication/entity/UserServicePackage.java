package ir.bontech.bontechapplication.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_service_package")
@Getter
@Setter
public class UserServicePackage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "rest_of_service_pack")
    private int restOfMaximumNumberOfTimesToUse;

    @OneToOne()
    @JoinColumn(name = "service_pack" , referencedColumnName = "id")
    private ServicePack servicePack;

    @ManyToOne
    @JoinColumn(name = "user_id" , nullable = false)
    private User user;

}
