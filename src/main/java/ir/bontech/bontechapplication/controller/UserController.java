package ir.bontech.bontechapplication.controller;

import ir.bontech.bontechapplication.entity.User;
import ir.bontech.bontechapplication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public String login(@RequestBody User user){
        String token = userService.generateToken(user);
        return token;
    }

    @PostMapping("/create")
    public User createUser(@RequestBody User user, @RequestHeader String token) {
        User savedUser = userService.saveNewUser(user, token);
        return savedUser;
    }

    @GetMapping("/list")
    public List<User> userList() {
        List<User> users = userService.findAll();
        return users;
    }

    @PostMapping("/edit/{id}")
    public User editUser(@PathVariable(value = "id") Long id, @RequestBody User user, @RequestHeader String token) {
        User editUser = userService.saveUser(id, user, token);
        return editUser;
    }

    @GetMapping("/delete/{id}")
    public User deleteUser(@PathVariable(value = "id") Long id, @RequestHeader String token){
        User deleteUser = userService.deleteUser(id,token);
        return deleteUser;
    }

}
