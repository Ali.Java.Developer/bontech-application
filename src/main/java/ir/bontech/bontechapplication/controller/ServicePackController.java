package ir.bontech.bontechapplication.controller;

import ir.bontech.bontechapplication.entity.ServicePack;
import ir.bontech.bontechapplication.service.ServicePackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/service")
public class ServicePackController {

    @Autowired
    private ServicePackService servicePackService;

    @PostMapping("/create")
    public ServicePack createServicePack(@Valid @RequestBody ServicePack servicePack, @RequestHeader String token) {
        ServicePack savedServicePack = servicePackService.saveNewServicePack(servicePack, token);
        return savedServicePack;
    }

    @PostMapping("/edit/{id}")
    public ServicePack editServicePack(@PathVariable(value = "id") Long id, @Valid @RequestBody ServicePack servicePack, @RequestHeader String token) {
        ServicePack editServicePack = servicePackService.editServicePack(id, servicePack, token);
        return editServicePack;
    }

    @GetMapping("/delete/{id}")
    public ServicePack deleteServicePack(@PathVariable(value = "id") Long id, @Valid @RequestHeader String token) {
        ServicePack deleteServicePack = servicePackService.deleteServicePack(id, token);
        return deleteServicePack;
    }

    @GetMapping("/list")
    public List<ServicePack> servicePackList() {
        List<ServicePack> servicePackList = servicePackService.findAll();
        return servicePackList;
    }

    @PostMapping("/active/{id}")
    public ServicePack activeServicePack(@PathVariable(value = "id") Long id, @ModelAttribute(value = "activeAt") String activeAt, @ModelAttribute(value = "hourToActive") int hourToActive, @RequestHeader String token) {
        ServicePack changedServicePack = servicePackService.setExpiredTime(id, activeAt, hourToActive, token);
        return changedServicePack;
    }

    @GetMapping("/disable/{id}")
    public ServicePack disableServicePack(@PathVariable(value = "id") Long id, @RequestHeader String token) {
        ServicePack disabledServicePacke = servicePackService.disableServicePack(id, token);
        return disabledServicePacke;
    }

    @GetMapping("/active")
    public List<ServicePack> activeServicePackList() {
        List<ServicePack> activeService = servicePackService.findServicePackByActive(true);
        return activeService;
    }

    @GetMapping("/allow")
    public List<ServicePack> AllowedServicePackList() {
        List<ServicePack> allowedService = servicePackService.findServicePackByAllowed(true);
        return allowedService;
    }


}
