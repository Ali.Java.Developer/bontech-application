package ir.bontech.bontechapplication.data;

public class Constraint {
    public final static int limitTimeToActiveServicePack = 24;
    public final static int maximumTimeForActiveAServicePack = 12;
}
