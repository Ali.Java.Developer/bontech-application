package ir.bontech.bontechapplication.data;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import java.security.Key;

@Component
public class Secret {
    public final static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
}
