package ir.bontech.bontechapplication.repository;

import ir.bontech.bontechapplication.entity.ServicePack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServicePackRepository extends JpaRepository<ServicePack , Long> {
    @Query(nativeQuery = true , value = "select * from bontech.service_pack where is_active = ?1")
    public List<ServicePack> findByActive(boolean active);

    @Query(nativeQuery = true , value = "select * from bontech.service_pack where is_allowed = ?1")
    public List<ServicePack> findByAllowed(boolean isAllowed);

//    @Query(nativeQuery = true , value = "select * from bontech.service_pack where is_active = 'true' and " +
//            "active_at like '?1/?2/?3%'")
//    public  List<ServicePack> findServicePackByActiveTrueAndActiveTime(int year , int month , int day);
}
