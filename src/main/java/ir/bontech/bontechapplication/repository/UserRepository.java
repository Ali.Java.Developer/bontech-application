package ir.bontech.bontechapplication.repository;

import ir.bontech.bontechapplication.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    public Optional<User> findUserByNameAndPassword(String name , String password);
    public Optional<User> findUserByName(String name);
}
